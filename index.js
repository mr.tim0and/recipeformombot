import TelegramApi from 'node-telegram-bot-api'
import translate from "translate";
import axios from "axios";
import dotenv from "dotenv";

dotenv.config()
translate.from = process.env.LANG_FROM_DEFAULT;
translate.to = process.env.LANG_TO_DEFAULT
const bot = new TelegramApi(process.env.TOKEN_TELEGRAM_BOT, {polling: true})

bot.on('message', async msg => {
    const text = await translate(msg.text, {from: 'ru', to: 'en'});
    const chatId = msg.chat.id;
    axios.get(process.env.URL_FOR_RECIPE_API, {
        params: {
            type: 'public',
            q: text.trim(),
            app_id: process.env.APP_ID_RECIPE_API,
            app_key: process.env.TOKEN_RECIPE_API,
            imageSize: 'LARGE'
        }
    }).then(async recipes => {
        if (recipes.data.hits.length > 0) {
            let recipe = recipes.data.hits[0].recipe;
            let photoUrl = recipe.images.LARGE.url;
            let ingredients = recipe.ingredientLines;
            console.log(recipe);
            if (ingredients.length > 0) {
                let textIngr = recipe.label.replace(/&/g, '') + "\n\n" + "Ingredients:\n\n";
                ingredients.forEach((item) => {
                    textIngr += ('* ' + item.replace(/&/g, '') + "\n");
                })
                textIngr += ('\nCalories: ' + recipe.calories)

                let message = await translate(textIngr);

                await bot.sendPhoto(chatId, photoUrl)
                await bot.sendMessage(chatId, message)
            }
        }
    }).catch(async () => {
        await bot.sendMessage(chatId, 'Ничего не найдено')
    });
})
